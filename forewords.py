import csv
import nltk
from nltk import FreqDist
from nltk.collocations import *
from dictionary import *

# categories based on the different study programmes
language = ["CIW", "J", "IK", "TW", "NTC", "NL", "SLP", "L", "ATW"]
humanities = ["G", "KG", "IBIO", "KCM", "AHA", "IBIO", "MHIR", "AT", "TCMO", "LCS", "A", "GLC", "KCW"]


# extract s-number, grade and study programma from CSV file
def grades():
	scores = []
	with open('scores.csv') as csvfile:
		reader = csv.DictReader(csvfile)
		for row in reader:
			number = row['S-number'] 
			grade = row['Grade']
			domain = row['Domain']
			data = [number, grade, domain]
			scores.append(data)
	return(scores)

# opens the foreword file and calculates the frequency of words using dictionary.py calc function
def read(snumber):
	with open (snumber+".txt", "r") as myfile:
	    data = myfile.read().replace('\n', ' ')

	words = nltk.word_tokenize(data)

	words=[word.lower() for word in words if word.isalpha()]

	length = (len(words))

	score = (calc(words, length))

	score.append(length)
	score.insert(0, snumber)


	return(score)
	
def main():
	grade = grades()

	# different categories of grading
	langbad = []
	langgood = []
	humbad = []
	humgood = []

	# determine category of thesis and calculate the frequencies of words
	for i in grade:
		if i[2] in language:
			if int(i[1]) == 6 or int(i[1]) == 7:
				langbad.append(read(i[0]))
			elif int(i[1]) == 8 or int(i[1]) == 9:
				langgood.append(read(i[0]))
		if i[2] in humanities:
			if int(i[1]) == 6 or int(i[1]) == 7:
				humbad.append(read(i[0]))
			elif int(i[1]) == 8 or int(i[1]) == 9:
				humgood.append(read(i[0]))

	# save the data in a list
	data = [langbad, langgood, humbad, humgood]

	# output the data for each category
	print("category;snumber;iwords;wewords;youwords;shehe;they;articles;prepositions;conjunctions;positive;negative;steun;begeleiding;afsluiten;academisch;bigwords;length")
	for line in langbad:
		print("1;", ";".join(str(x) for x in line))
	for line in langgood:
		print("2;", ";".join(str(x) for x in line))
	for line in humbad:
		print("3;", ";".join(str(x) for x in line))
	for line in humgood:
		print("4;", ";".join(str(x) for x in line))

	
main() 