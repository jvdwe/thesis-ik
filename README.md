# How to use

Place forewords.py and dictionary.py in a directory with prefaces saved as [studentnumber].txt (without S!) and 
a scores.csv file containing the S-number, grade, title of thesis and study programme. 
Run
	

```
#!shell

python3 forewords.py > results.csv

```

to save your results.

Note:
Since the original grades and prefaces are confidential information an example of a preface file and CSV file are provided.

NB: In an older version, prefaces were named forewords and vocabularies were called dictionaries. There are still traces of this old labeling in the current code.