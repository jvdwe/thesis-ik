iwords = ['ik', 'mij', 'mijn', 'me']
wewords = ['wij', 'we', 'onze', 'ons']
youwords = ['jij', 'u', 'uw', 'jouw', 'jullie']
shehe = ['zij', 'hij', 'haar']
they = ['zij', 'ze', 'hun', 'hen']
articles = ['het', 'de', 'een']

prepositions = ['à', 'ingevolge', 'qua', 'aan', 'inzake', 'aangaande', 'richting', 
'achter', 'jegens', 'rond', 'af', 'rondom', 'krachtens', 'behoudens', 'sedert', 
'beneden', 'langs', 'sinds', 'benevens', 'luidens', 'staande', 'benoorden', 'beoosten', 
'met', 'te', 'betreffende', 'tegen', 'bewesten', 'na', 'tegenover', 'bezijden', 'naar', 
'tijdens', 'bezuiden', 'naast', 'tot', 'bij', 'nabij', 'trots', 'binnen', 'namens', 'tussen', 
'blijkens', 'niettegenstaande', 'boven', 'nopens', 'uit', 'buiten', 'om', 'van', 'conform', 
'omstreeks', 'vanaf', 'contra', 'omtrent', 'vanuit', 'ondanks', 'vanwege', 'door', 'onder', 'via', 
'ongeacht', 'volgens', 'gedurende', 'onverminderd', 
'voor', 'gezien', 'op', 'voorbij', 'over', 'overeenkomstig', 'wegens', 'in', 'per', 'zonder']

conjunctions = ['en', 'of', 'alsof', 'maar', 'doch', 
'noch', 'dus', 'derhalve', 'daarom', 'doordat', 'door', 'terwijl', 'omdat', 'aangezien', 'want', 'daar', 'dewijl', 'doordien', 'naardien', 
'nademaal', 'overmits', 'vermits', 'wijl', 'indien', 'ingeval', 'zo', 'zodat', 'opdat', 'sinds', 'sedert', 'nadat', 
'dat', 'vooraleer', 'voor', 'aleer', 'eer', 'voordat', 'totdat', 'toen', 'zodra', 'als', 'zoals', 'als', 'dan', 'zonder', 
'dat', 'behalve', 'al', 'alhoewel', 'hoewel', 'ofschoon', 'schoon', 'mits', 'tenware', 'tenzij', 
'naar', 'naargelang', 'naarmate', 'wanneer']


positive = ['tevreden', 'volwassen', 'fijne', 'fijn', 
'degelijke', 'degelijk', 'verrassen', 'vernieuwende', 'nuttige', 'waardevolle', 'waardevol', 'plezierig',
'plezierige', 'geïnteresseerd', 'prettige', 'prettig', 'zekere', 'blije', 'blij',
'heldere', 'helder', 'aanstekelijke','aanstekelijk', 'originele', 'origineel', 'heerlijke', 'heerlijk', 'geweldige', 'geweldig',
'onbezorgd', 'onbezorgd', 'geïnspireerd', 'uitstekende', 'uitstekend', 'interesse', 'vakkundige', 'vakkundig', 
'aanstekelijk', 'aanstekelijke', 'leuke', 'leuk', 'plezier', 'interessante', 'interessant', 'briljant', 'briljante', 'inspirerende', 'inspirerend']

negative = ['bekruipt', 'schijn', 'bedompte', 'getwijfeld', 'bloedde'
'gemengd', 'gemengde', 'loodjes', 'uitdagingen', 'stress',
'twijfel', 'geduldig', 'geduldige' 'vergeven', 'intensief', 'vervelend', 'vervelende',
'poging', 'klagen', 'klaagzang', 'klaagzangen', 'uitdaging', 'doorzetten', 'moeilijk', 'moeilijke', 'hobbels', 'pittig', 'pittige', 'alhoewel', 'gestreste']


steun = ['steun', 'steunde', 'steunen', 'opbeurende', 'bijstaan', 'bijgestaan', 'doorzetten', 'aansporing', 'doorheen', 
'vergeven', 'doorzettingsvermogen', 'ondersteund', 'aansporen', 'familie', 'duwtje', 'support', 'ondersteuning', 'familieleden',
'vrienden', 'bemoedigingen', 'bemoediging', 'verschuldigd']


begeleiding = ['begeleiding', 'belangstelling', 'enthousiasmeren', 'bijdragen', 'verbeteren', 
'instructies', 'aantekeningen', 'begeleid', 'begeleidt', 'begeleiden', 'kritisch', 'kritische', 'reacties', 'luisteren',
'luisterend', 'commentaar', 'bereidheid', 'feedback']

afsluiten = ['studententijd', 'afsluiten', 'fulltime', 'gecombineerd', 'balans', 'feestjes',
'afstudeeropdracht', 'geduurd', 'opluchting', 'afstudeertraject', 'baan', 'eindstreep', 'moest',
'moesten', 'scriptieperiode', 'eindresultaat', 'gekost', 'afgestudeerd', 'studentenleven', 'colleges', 'tussendoor'
]

academisch = ['rapportage', 'research', 'termen', 'niveau', 'bestuderen', 'belangstelling', 'verdiept', 'verdiepte'
'planmatig', 'bijdragen', 'tijdschrift', 'factoren', 'effectiviteit', 'toegepast', 'resultaten', 
'conceptversie', 'wetenschappelijk', 'complexe', 'logische', 'statement', 'streven', 'combinatie',
'afstudeeronderzoek', 'rapporteren', 'rapport', 'onderzoeksartikel', 'universitair', 'universitaire',
'concept', 'onderzoekscoördinator', 'materialen', 'anonimiteit', 'artikel', 'methodologisch', 'totstandkoming', 'onderzoeksveld']

# contains all the dictionaries for iteration
dictos = [iwords, wewords, youwords, shehe, they, articles, prepositions, conjunctions,
positive, negative, steun, begeleiding, afsluiten, academisch]

# counts the frequency of each word in each dictionary and calculates percentage of total words
def calc(words, length):
	scores = []
	for dic in dictos:
		n = 0
		for word in words:
			if word in dic:
				n = n + 1
		score = (n / length) * 100

		scores.append(score)

	x = 0
	for word in words:
		if len(word) > 6:
			x = x + 1
	scores.append((x /length) * 100)
	return scores


